/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {

    public static void main(String[] args) {

        //Conjunto<Integer> c2=new Conjunto(20);
        //Conjunto<Integer> c3=new Conjunto(30);
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
        try {
            Conjunto<Integer> c1 = new Conjunto(3);
            Conjunto<Integer> c2 = new Conjunto(4);
            c1.adicionarElemento(1);
            c1.adicionarElemento(5);
            c1.adicionarElemento(7);
            c2.adicionarElemento(1);
            c2.adicionarElemento(5);
            c2.adicionarElemento(7);
             c2.adicionarElemento(2);
            
           
            System.out.println("Conjunto Normal");
            System.out.print(c1.toString() + " ");
            System.out.println("Conjunto Ordenado");
            System.out.println("concatenar restrictivo");
            c1.concatenar(c2);
            c1.ordenar();
            c1.remover(5);
            c1.remover(5);
           // c1.concatenarRestrictivo(c2);
            System.out.println(c1.toString());
            //c1.ordenarBurbuja();
            /*c1.ordenarBurbuja();

            System.out.println(c1.toString());
            System.out.println("mayor elemento");
            System.out.println(c1.getMayorElemento());*/
            //c1.remover(1);
            //sc1.remover(13);
            //System.out.println("concatenar");
            //c1.concatenar(c2);
            //System.out.println(c1.toString());
            
            /*System.out.println("remover elemento 1");
            c1.remover(1);
            System.out.println(c1.toString());
             System.out.println("remover elemento 2");
            c1.remover(2);
            System.out.println(c1.toString());
             System.out.println("remover elemento 4");
            c1.remover(4);
            System.out.println(c1.toString());*/

            //System.out.print(c1.toString()+" ");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

}
