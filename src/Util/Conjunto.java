/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar() throws Exception {
        if (this.cajas == null) {
            throw new Exception("No se puede ordenar");
        }
        
        for (int j = 0; j < getLength()-1; j++) {
            int min=j;
            for (int k = j+1; k < getLength(); k++) {
                Comparable cajak= (Comparable) cajas[k].getObjeto();
                int rta= cajak.compareTo(cajas[min].getObjeto());
                if (rta<0) {
                    min=k;
                }
                
            }
            if(j!=min)
            {
                
                Caja<T> aux=  cajas[j];
                cajas[j]=cajas[min];
                cajas[min]= aux;
                
            }
            
        }
        
       
            
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja() throws Exception {
        if (this.cajas == null) {
            throw new Exception("No se puede ordenar");
        }
        for (int j = 0; j < getLength(); j++) {
            for (int k = 0; k < getLength()-j-1; k++) {
                Comparable cajak= (Comparable) cajas[k+1].getObjeto();
                int rta= cajak.compareTo(cajas[k].getObjeto());
                if (rta<0) {
                     
                   Caja<T> aux = cajas[k+1];
                    cajas[k+1]=cajas[k];
                    cajas[k]= aux;
                }
                
            }
            
        }
    }
    
   

    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     */
    private boolean existeElementoABorrar(T x)
    {
    
    
        for (int j = 0; j < cajas.length; j++) {
           Comparable obj=(Comparable) cajas[j].getObjeto();
          
            if (obj.compareTo(x)==0) {
                return true;
            }
            
            
        }
        
        
        return false;
    }
    public void remover(T objBorrado) throws Exception
    {    //aux= new Caja[cajas.length];
        if (!existeElementoABorrar(objBorrado)) {
            throw  new Exception("No existe el elemento a borrar");
        }
        Caja<T>[] aux=cajas;
    
        for (int j = 0; j < aux.length; j++) {
            Comparable obj=(Comparable) cajas[j].getObjeto();
            int rta=obj.compareTo(objBorrado);
            if (rta==0) {
                Caja<T> aux1=aux[j];
                aux[j]=aux[aux.length-1];
                aux[aux.length-1]=aux1;
            }
            
        }
        
        this.cajas=new Caja[aux.length-1];
        
        for (int j = 0; j < cajas.length; j++)cajas[j]=aux[j];
            
        
        ordenar();
    
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)throws Exception {
        if (nuevo == null) {
            throw new Exception("No se puede Concatenar, conjunto vacio");
        }
        
        
        Caja<T> aux[]= new Caja[this.cajas.length+nuevo.cajas.length];
        for (int j = 0; j < cajas.length; j++)
        {
            aux[j]=cajas[j];
        }
        for (int j = 0; j < nuevo.cajas.length; j++)
        {
            aux[cajas.length+j]=nuevo.cajas[j];
            this.i++;
        }
        this.cajas=aux;
        nuevo.removeAll();
      
    }
        
        
    
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso SI toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    
    
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception
    {       
            if (nuevo.getCapacidad()!=nuevo.getLength()) {
            throw new Exception("No se puede concatenar, no està lleno el Conjunto nuevo ");
        }
           
        for (int j = 0; j < cajas.length; j++) 
        {
            
            for (int k= 0; k< nuevo.getLength(); k++) 
            {
                
                Comparable cajaN= (Comparable) nuevo.cajas[k].getObjeto();
                
                if (cajaN.compareTo(cajas[j].getObjeto())==0) {
                    nuevo.remover(nuevo.cajas[k].getObjeto());
                    nuevo.i--;
                }
         
            }
        }   
        concatenar(nuevo);
        nuevo.removeAll();
    
    }

    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(Caja c:this.cajas)
            msg+=c.getObjeto().toString()+"\n";
        
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int j=1;j<this.getCapacidad();j++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[j].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }

    
}
